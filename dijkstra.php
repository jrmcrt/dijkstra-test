<?php 

$visited;
$nodes = array_fill(1, 6, ['weight' => PHP_INT_MAX]);//initialize weight value to 'infinite'
$route = array_fill(1, 6, 'NONE');//to keep track
$source = $origin = 1;//origin node
$destination = 6;//destination node
$nodes[$source]['weight'] = 0;//set source weight to 0
$nodes[1][2] = 3;//distance from node 1 to node 2 -> 3
$nodes[1][3] = 4;
$nodes[1][4] = 2;
$nodes[2][1] = 3;
$nodes[2][3] = 4;
$nodes[2][5] = 2;
$nodes[3][1] = 4;
$nodes[3][2] = 4;
$nodes[3][5] = 6;
$nodes[4][1] = 2;
$nodes[4][5] = 1;
$nodes[4][6] = 4;
$nodes[5][2] = 2;
$nodes[5][3] = 6;
$nodes[5][4] = 1;
$nodes[5][6] = 2;
$nodes[6][5] = 2;
$nodes[6][4] = 4;

while($source){//there's still unvisited nodes
	foreach(getNeigbour($nodes[$source]) as $k => $v){
		//$nodes[$k]['weight'] = min($v + $nodes[$source]['weight'], $nodes[$k]['weight']);
		if($v + $nodes[$source]['weight'] < $nodes[$k]['weight']){//if current node weight + distance node/neighbor < neighbor weight
			$nodes[$k]['weight'] = $v + $nodes[$source]['weight'];//update neighbor weight
			$route[$k] = $source; //shortest path to k node is throught source node
		}
	} 	

	$visited[] = $source; //mark node as visited
	$source = getMinWeightNode($nodes, $visited);//get unvisited node with lowest weight value
}

function getNeigbour($node){
	return array_filter($node, function($v){return $v != 'weight';}, ARRAY_FILTER_USE_KEY);//filter out weight property
}

function getMinWeightNode($nodes, $visited){
	$r_nodes = array_filter($nodes, function($key) use($visited){//filter out visited nodes
		return !in_array($key, $visited);
	}, ARRAY_FILTER_USE_KEY);

	uasort($r_nodes, function($a, $b)//sort nodes by weight asc
	{
		return strcmp($a['weight'], $b['weight']);
	});

	return array_search(reset($r_nodes), $r_nodes);//return index of the first element
}

$path = [$destination];
$current = $destination;

while(is_numeric($route[$current])){//backtrack
	$path[] = $route[$current]; 
	$current = $route[$current];
}

echo "Minimum distance from $origin to $destination is : " . $nodes[$destination]['weight'] . "\n" . join(' -> ', array_reverse($path)) . "\n";